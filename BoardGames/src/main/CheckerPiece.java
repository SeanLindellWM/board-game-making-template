package main;

import java.awt.image.BufferedImage;

public class CheckerPiece implements GamePiece{
	
	private int xPosition;
	private int yPosition;
	PieceColor myColor;
	PieceType myType;
	BufferedImage myImage;
	
	public CheckerPiece(PieceColor color, PieceType type, BufferedImage image) {
		myColor = color;
		myType = type;
		myImage = image;
	}
	
	public CheckerPiece(int x, int y, PieceColor color, PieceType type, BufferedImage image) {
		xPosition = x;
		yPosition = y;
		myColor = color;
		myType = type;
		myImage = image;
	}

	@Override
	public PieceType getPieceType() {
		return myType;
	}

	@Override
	public PieceColor getPieceColor() {
		return myColor;
	}

	@Override
	public BufferedImage getPieceImage() {
		return myImage;
	}

	@Override
	public int getX() {
		return xPosition;
	}

	@Override
	public int getY() {
		return yPosition;
	}
	
	@Override
	public void setX(int x) {
		xPosition = x;
	}

	@Override
	public void setY(int y) {
		yPosition = y;
	}

	@Override
	public boolean isValidMove(int x, int y, Board board) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean move(int x, int y, Board board) {
		// TODO Auto-generated method stub
		return false;
	}

}
