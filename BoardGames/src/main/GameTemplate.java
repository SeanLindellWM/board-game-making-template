package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 * This class is can be used as a template for creating any kind of board game. Either
 * create your own class which inherits from GameTemplate, or just refactor GameTemplate
 * to rename it to whatever game you're trying to make and work from there.
 * @author Sean Lindell
 * @author Your name here
 */
public class GameTemplate extends JPanel{
	
	private static final long serialVersionUID = 1L;
	
	// Window Information
	static String windowTitle = "Test Game";
	static int windowWidth = 800;
	static int windowHeight = 800;
	
	// Colors
	Color backgroundColor = Color.lightGray;
	Color lineColor = Color.black;
	Color evenBoardSpacesColor = new Color(240, 230, 190);
	Color oddBoardSpacesColor = new Color(55,55,55);
	Color boardBoarderColor = new Color(40,30,12);
	
	// Board Data
	Board gameBoard = new Board();
	int boardWidthPx = 400;
	int boardHeightPx = 400;
	int boardXPosition = 20;
	int boardYPosition = 20;
	int boardBoarderThickness = 5;
	
	// Images
	final BufferedImage blackCheckerPeice = loadImage("BlackCheckerPiece.png");
	final BufferedImage blackCheckerPeiceKing = loadImage("BlackCheckerPieceKinged.png");
	final BufferedImage redCheckerPeice = loadImage("RedCheckerPiece.png");
	final BufferedImage redCheckerPeiceKing = loadImage("RedCheckerPieceKinged.png");
	
	/**
	 * Constructor method for the game. By default only adds a mouse listener for user input, but other
	 * methods of obtaining user input can be added here as needed.
	 */
	public GameTemplate() {
		addMouseListener(myMouseAdapter);
	}
	
	// Graphics methods
	
	/**
	 * Call this method by calling the repaint() method. Used to draw the graphics on the frame whenever the
	 * graphics need to be changed. Works by painting over the frame with the background color, then adding
	 * other desired graphical elements such as a game board or text.
	 * @param g the graphics object being drawn on. Use this to draw graphical elements through methods such as 
	 * g.drawLine(), g.drawImage(), or g.drawString().
	 */
	@Override
	public void paint(Graphics g) {
		clearCanvas(g);
		gameBoard.drawBoard(g, boardXPosition, boardYPosition, boardHeightPx, boardWidthPx, boardBoarderThickness, 
				evenBoardSpacesColor, oddBoardSpacesColor, lineColor, boardBoarderColor);
	}
	
	/**
	 * Clears the canvas by putting a filled rectangle over the whole window with the background color.
	 * @param g the graphics object to clear
	 */
	private void clearCanvas(Graphics g) {
		g.setColor(backgroundColor);
		g.fillRect(0, 0, this.getSize().width, this.getSize().height);
	}
	
	// Special resource methods
	
	/**
	 * This method produces a BufferedImage from an image name. Images you would like to load 
	 * using this method must be placed in the res/images folder. Use this method to prep images 
	 * for the g.drawImage() method where variables are initialized. As an example, the images 
	 * for checkers pieces are initialized in the default GameTemplate.java example file.
	 * @param fileName The name of the file of the image you would like to load. Note that you must 
	 * include the extension (e.x.: .jpg, .png) and that some image types may not be natively supported. 
	 * @return The bufferedImage of the requested image. This can then be used for g.drawImage();
	 */
	public BufferedImage loadImage(String fileName) {
		BufferedImage loadedImage = null;
		Path imagePath = Paths.get("res", "images",fileName);
		try {
			loadedImage = ImageIO.read(new File(imagePath.toString()));
		} catch (Exception e) {
			System.out.println("The image '" + fileName + "' is not in the directory");
		}
		return loadedImage;
	}
	
	/**
	 * This method plays an audio file located in res/audio. The method directly plays the sound as 
	 * soon as called with no delay by default. Note that this is only compatible with .wav sounds, 
	 * and that you must include the ".wav" extension as part of the input string.
	 * @param fileName The name of the audio file to play, including the file extension.
	 * @return The Clip of the audio being played is returned so that the audio can be adjusted before 
	 * it finishes playing if desired. This could include making the sound loop, pausing the sound, 
	 * or skipping to a specific point in the sound.
	 */
	public Clip playAudio(String fileName) {
		Clip clip = null;
		Path clipPath = Paths.get("res", "audio",fileName);
		File audioFile = clipPath.toFile();
		if (!audioFile.exists()) {
			System.out.println("The audio clip '" + fileName + "' is not in the directory");
			return null;
		}
		try {
			AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
			clip = AudioSystem.getClip();
			clip.open(audioStream);
			clip.setFramePosition(0);
			clip.start();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return clip;
	}
	
	/**
	 * The main method just sets up the frame for the board game. This includes setting the frame's
	 * size, adjusting its settings for closing/resizing, and adding the board game component to the frame.
	 * @param args Unused, can be safely ignored
	 */
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setTitle(windowTitle);
		frame.setSize(windowWidth,windowHeight);
		frame.setResizable(false);
		frame.add(new GameTemplate());
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	/**
	 * The myMouseAdapter tracks the location of the mouse on the screen and when the mouse clicks
	 * so that the user can interact with the game. The internal method mousePressed() is called
	 * whenever the user clicks anywhere in the window. e.getX() and e.getY() can then be used to
	 * get the x and y coordinates of the mouse on the frame where (0,0) is the top left of the 
	 * screen. Board.getPieceAt(e) can be used to get the square the player has clicked on if the
	 * board has been drawn on the screen. Just be sure to write the line 
	 * addMouseListener(myMouseAdapter) to the constructor method for the game class.
	 */
	MouseAdapter myMouseAdapter = new MouseAdapter() {
		@Override
		public void mousePressed(MouseEvent e) {
			//TODO As a placeholder for testing, places a checker piece where you click on the board
			gameBoard.placePieceAtMouse(e, new CheckerPiece(PieceColor.BLACK, PieceType.CHECKER, blackCheckerPeice));
			repaint();
		}
	};
}
