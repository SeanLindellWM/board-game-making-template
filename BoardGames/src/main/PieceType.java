package main;

/**
 * PieceType is used to differentiate different types of pieces. E.x.: PAWN, KNIGHT, QUEEN, CHECKER, CHECKERKING
 * Add piece types as needed for making games other than Tic-Tac-Toe, Checkers, and Chess.
 * @author Sean Lindell
 */
public enum PieceType {
	EMPTY,
	X,
	O,
	CHECKER,
	CHECKERKING,
	PAWN,
	ROOK,
	KNIGHT,
	BISHOP,
	QUEEN,
	KING
}
