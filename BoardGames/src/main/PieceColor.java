package main;

/**
 * PieceColor is used to differentiate different colors of pieces. E.x.: BLACK, WHITE, QUEEN, BLUE, RED, GREEN, PINK.
 * Do not confuse these colors with the constants in the Color class. (E.x.: Color.BLACK).
 * PieceColor doesn't strictly only need to be colors. It's just used to differentiate which player controls 
 * each piece.
 * @author Sean Lindell
 */
public enum PieceColor {
	BLACK,
	WHITE,
	BLUE,
	RED,
	YELLOW,
	GREEN,
	PURPLE,
	ORANGE,
	PINK
}
