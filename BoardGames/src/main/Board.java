package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;

/**
 * This class is responsible for storing the GamePieces that are on the board using a 2D array, and
 * drawing the board using a Graphics object from GameTemplate. The default board size is 8x8,
 * but boards of any size can be created using the Board constructor method. Note that methods in 
 * the class dependent on mouse position should only be called after the Board has been drawn using
 * the drawBoard() method at least once. 
 * @author Sean Lindell
 * @author Your name here
 */
public class Board {
	private GamePiece[][] gameBoard;
	private int boardWidth;
	private int boardHeight;
	private int spaceWidth;
	private int spaceHeight;
	private int xPosition;
	private int yPosition;
	
	/**
	 * Default constructor class for board object. Defaults to size 8x8 as that's the size for chess/checkers
	 * boards.
	 */
	public Board() {
		gameBoard = new GamePiece[8][8];
		boardWidth = 8;
		boardHeight = 8;
	}
	
	/**
	 * Constructor class for board object which produces a board with the given dimensions.
	 * @param boardWidth The width of the board. Must be greater than zero.
	 * @param boardHeight The height of the board. Must be greater than zero.
	 */
	public Board(int boardWidth, int boardHeight) {
		gameBoard = new GamePiece[boardWidth][boardWidth];
		this.boardWidth = boardWidth;
		this.boardHeight = boardHeight;
	}
	
	/**
	 * Getter method for the width of the board in spaces
	 * @return The number of spaces wide the board is
	 */
	public int getBoardWidth() {
		return boardWidth;
	}
	
	/**
	 * Getter method for the height of the board in spaces
	 * @return The number of spaces high the board is
	 */
	public int getBoardHeight() {
		return boardHeight;
	}
	
	/**
	 * Getter method for the 2D array which stores the contents of the board.
	 * In general avoid using this method unless you absolutely have to. It's much 
	 * better to use the getters and setters for individual spaces.
	 * @return The array containing the contents of the board.
	 */
	public GamePiece[][] getGameBoard() {
		return gameBoard;
	}
	
	/**
	 * Setter method for placing a piece on the given space. Will override
	 * whatever piece was there beforehand
	 * @param x horizontal coordinate
	 * @param y vertical coordinate
	 * @param piece the piece to place
	 * @return the GamePiece that was previously in the space being placed in
	 */
	public GamePiece placePieceAt(int x, int y, GamePiece piece) {
		GamePiece temp = gameBoard[x][y];
		gameBoard[x][y] = piece;
		piece.setX(x);
		piece.setY(y);
		return temp;
	}
	
	/**
	 * Setter method for placing a piece on the space the mouse is over. Call
	 * drawBoard at least once before using this method.
	 * @param e MouseEvent object used to find the current mouse position
	 * @param piece GamePiece to place on the board
	 * @return the GamePiece that was previously in the space. Returns null if
	 * the user did not click on the board
	 */
	public GamePiece placePieceAtMouse(MouseEvent e, GamePiece piece) {
		int column = getColumnOfMouse(e);
		int row = getRowOfMouse(e);
		if (row!= -1 && column!= -1) {
			GamePiece temp = gameBoard[column][row];
			gameBoard[getColumnOfMouse(e)][getRowOfMouse(e)] = piece;
			piece.setX(column);
			piece.setY(row);
			return temp;
		}
		return null;
	}
	
	/**
	 * Getter method for the piece at the given board coordinates. E.x. 0,0 is the top left space
	 * on the board
	 * @param x Coordinate of the piece. Must be from 0 to boardLength-1
	 * @param y Coordinate of the piece. Must be from 0 to boardHeight-1
	 * @return The piece at specified coordinates.
	 */
	public GamePiece getPieceAt(int x, int y) {
		return gameBoard[x][y];
	}
	
	/**
	 * Given a mouse object, this method returns the piece in the space where the player has
	 * clicked. This method should not be used if drawBoard() hasn't been run at least once.
	 * @param e The mouseEvent given as a parameter in the mousePressed() method.
	 * @return The piece on the space where the player clicked. Returns null if the player did
	 * not click on the board.
	 */
	public GamePiece getPieceAt(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		int column = -1;
		int row = -1;
		int marker = xPosition;
		for (int i = 0; i < boardWidth; i++) {
			if(mouseX>marker && mouseX<marker+spaceWidth) {
				column = i;
			}
			marker += spaceWidth;
		}
		marker = yPosition;
		for (int i = 0; i < boardHeight; i++) {
			if(mouseY>marker && mouseY<marker+spaceHeight) {
				row = i;
			}
			marker += spaceHeight;
		}
		if (column==-1 || row==-1) {
			return null;
		}
		return gameBoard[column][row];
	}
	
	/**
	 * Given a MouseEvent object, determines which column of squares the user clicked on.
	 * Can only be used after drawBoard() has been called at least once.
	 * @param e The mouseEvent given as a parameter in the mousePressed() method.
	 * @return The index of the column the user clicked on. Returns -1 if the user's 
	 * click doesn't line up with a column.
	 */
	public int getColumnOfMouse(MouseEvent e) {
		int mouseX = e.getX();
		int column = -1;
		int marker = xPosition;
		for (int i = 0; i < boardWidth; i++) {
			if(mouseX>marker && mouseX<marker+spaceWidth) {
				column = i;
			}
			marker += spaceWidth;
		}
		return column;
	}
	
	/**
	 * Given a MouseEvent object, determines which row of squares the user clicked on.
	 * Can only be used after drawBoard() has been called at least once.
	 * @param e The mouseEvent given as a parameter in the mousePressed() method.
	 * @return The index of the row the user clicked on. Returns -1 if the user's 
	 * click doesn't line up with a row.
	 */
	public int getRowOfMouse(MouseEvent e) {
		int mouseY = e.getY();
		int row = -1;
		int marker = yPosition;
		for (int i = 0; i < boardHeight; i++) {
			if(mouseY>marker && mouseY<marker+spaceHeight) {
				row = i;
			}
			marker += spaceHeight;
		}
		return row;
	}
	
	/**
	 * Given a MouseEvent, determines whether the mouse is somewhere on the board.
	 * Can only be used after drawBoard() has been called at least once.
	 * @param e  The mouseEvent given as a parameter in the mousePressed() method.
	 * @return true if mouse is on board, false if not
	 */
	public boolean mouseOnBoard(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		boolean xAligned = false;
		boolean yAligned = false;
		if (xPosition<mouseX && mouseX<xPosition+(boardWidth*spaceWidth)) {
			xAligned = true;
		}
		if (yPosition<mouseY && mouseY<yPosition+(boardHeight*spaceHeight)) {
			yAligned = true;
		}
		if (xAligned&&yAligned) {
			return true;
		}
		return false;
	}
	
	// TODO commenting for drawBoard method once it's fully completed
	public void drawBoard(Graphics g, int x, int y, int height, int width, int boarderThickness, Color oddSpacesColor, Color evenSpacesColor, 
			Color lineColor, Color boarderColor) {
		xPosition = x;
		yPosition = y;
		spaceWidth = width/boardWidth;
		spaceHeight = height/boardHeight;
		
		// Draw the board boarder
		g.setColor(boarderColor);
		g.fillRect(x-boarderThickness, y-boarderThickness, width+boarderThickness*2, height+boarderThickness*2);
		
		// Drawing the board squares
		for (int i = 0; i < boardHeight; i++) {
			for (int j = 0; j < boardWidth; j++) {
				if((i+j)%2==0) {
					g.setColor(evenSpacesColor);
				}
				else {
					g.setColor(oddSpacesColor);
				}
				g.fillRect(x+i*spaceWidth, y+j*spaceHeight, spaceWidth, spaceHeight);
			}
		}
		
		// Drawing the board lines
		g.setColor(lineColor);
		for (int i = 0; i < boardWidth+1; i++) {
			g.drawLine(x+(i*spaceWidth), y, x+(i*spaceWidth), y+height);
		}
		for (int i = 0; i < boardHeight+1; i++) {
			g.drawLine(x, y+(i*spaceHeight), x+width, y+(i*spaceHeight));
		}
		
		// Drawing the pieces
		for (int i = 0; i < boardHeight; i++) {
			for (int j = 0; j < boardWidth; j++) {
				if (gameBoard[i][j]!=null) {
					GamePiece piece = this.getPieceAt(i,j);
					g.drawImage(piece.getPieceImage(), x+i*spaceWidth, y+j*spaceHeight, spaceWidth, spaceHeight, null);
				}
			}
		}
	}
}
