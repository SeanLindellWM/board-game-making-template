/**
 * 
 */
package main;

import java.awt.image.BufferedImage;

/**
 * @author Sean Lindell
 *
 */
public interface GamePiece {
	
	/**
	 * Getter method for piece type
	 * @return type of piece
	 */
	public PieceType getPieceType();
	
	/**
	 * Getter method for piece color
	 * @return color of piece
	 */
	public PieceColor getPieceColor();
	
	/**
	 * Getter method for the image associated with a given piece
	 * @return the image to use for the piece
	 */
	public BufferedImage getPieceImage();
	
	/**
	 * Getter method for piece coordinates
	 * @return x index, 0 to board width 
	 */
	public int getX();
	
	/**
	 * Getter method for piece coordinates
	 * @return y index, 0 to board height
	 */
	public int getY();
	
	/**
	 * Setter method for piece coordinates
	 * @param x index, 0 to board width 
	 */
	public void setX(int x);
	
	/**
	 * Setter method for piece coordinates
	 * @param y index, 0 to board height
	 */
	public void setY(int y);
	
	/**
	 * Method for determining if moving to the inputed square would be a valid move
	 * @param x index of proposed move
	 * @param y index of proposed move
	 * @param board a reference to the board
	 * @return returns true if the move is valid, false if it is not
	 */
	boolean isValidMove(int x, int y, Board board);
	
	/**
	 * Method for moving the piece
	 * @param x index of proposed move, 0 to board width
	 * @param y index of proposed move, 0 to board height
	 * @param board a reference to the board
	 * @return returns true if the move was successfully made, returns false otherwise
	 */
	boolean move(int x, int y, Board board);
}
